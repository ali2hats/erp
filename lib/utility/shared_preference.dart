import 'package:shared_preferences/shared_preferences.dart';

getToken() async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'token';
  return prefs.get(key) ?? '0';
}

setToken(token) async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'token';
  final value = token;
  prefs.setString(key, value);
}

getAccountType() async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'account_type';
  return prefs.get(key) ?? '0';
}

setAccountType(accounType) async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'account_type';
  final value = accounType;
  prefs.setString(key, value);
}

getUserId() async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'userid';
  return prefs.get(key) ?? '0';
}

setUserId(userid) async {
  final prefs = await SharedPreferences.getInstance();
  final key = 'userid';
  final value = userid;
  prefs.setString(key, value);
}
