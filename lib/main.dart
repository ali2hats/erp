import 'package:erp_app/pages/home/home.dart';
import 'package:erp_app/pages/splash/splash.dart';
import 'package:erp_app/pages/task/task.dart';
import 'package:erp_app/pages/task/task_home.dart';
import 'package:flutter/material.dart';
import 'pages/login/login.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static final baseUrl = "http://192.168.1.9:8012/ali/";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        Splash.myRoute: (BuildContext context) => Splash(),
        LogIn.myRoute: (BuildContext context) => LogIn(),
        Home.myRoute: (BuildContext context) => Home(),
        TaskHome.myRoute: (BuildContext context) => TaskHome(),
        Task.myRoute: (BuildContext context) => Task(),
      },
    );
  }
}
