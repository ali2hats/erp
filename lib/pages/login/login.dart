import 'package:erp_app/pages/home/home.dart';
import 'package:erp_app/utility/shared_preference.dart';
import 'package:erp_app/utility/snak_bar.dart';
import 'package:flutter/material.dart';
import 'widget/text_field.dart';
import 'package:http/http.dart' as http;
import '../../main.dart';
import 'dart:convert';

class LogIn extends StatefulWidget {
  static const String myRoute = '/login';
  @override
  State<StatefulWidget> createState() {
    return _LogInState();
  }
}

class _LogInState extends State<LogIn> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final mainColor = Colors.blue;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        margin: EdgeInsets.all(20),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              Text(
                'Log In',
                style: TextStyle(color: mainColor, fontSize: 25),
              ),
              SizedBox(
                height: 15,
              ),
              _buildForm()
            ],
          ),
        ),
      ),
    );
  }

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  Widget _buildForm() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _buildUsername(),
          SizedBox(
            height: 20,
          ),
          _buildPassword(),
          SizedBox(
            height: 20,
          ),
          _submitButton()
        ],
      ),
    );
  }

  final _usernameController = TextEditingController();
  final _usernameFocusNode = FocusNode();
  onValidateUsername(String value) {
    if (value.isEmpty) {
      return 'Empty username';
    }
    return null;
  }

  Widget _buildUsername() {
    return buildTextField(
        label: "Username",
        cFocusNode: _usernameFocusNode,
        cController: _usernameController,
        cValidator: onValidateUsername);
  }

  final _passwordController = TextEditingController();
  final _passwordFocusNode = FocusNode();
  onValidatePassword(String value) {
    if (value.isEmpty) {
      return 'Empty password';
    }
    return null;
  }

  Widget _buildPassword() {
    return buildTextField(
        label: "Password",
        cObsecure: true,
        cFocusNode: _passwordFocusNode,
        cController: _passwordController,
        cValidator: onValidatePassword);
  }

  Widget _submitButton() {
    return RaisedButton(
      color: mainColor,
      onPressed: () {
        if (!_formKey.currentState.validate()) {
          print('Validate Triggered');
          return;
        }
        _formKey.currentState.save();
        _performLogIn();
      },
      child: Text(
        'Submit',
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  _performLogIn() async {
    print('Username :' + _usernameController.text);
    print('Password : ' + _passwordController.text);

    String myUrl = MyApp.baseUrl + "login.php";
    final response = await http.post(myUrl, headers: {
      'Accept': 'application/json'
    }, body: {
      "username": _usernameController.text,
      "password": _passwordController.text
    });
    var status = response.body.contains('error');

    var data = json.decode(response.body);

    if (status) {
      print('data : ${data["error"]}');
      showSnackBar(_scaffoldKey, 'An Error Occured');
    } else {
      print('data : ${data["token"]}');
      await setToken(data["token"]);
      await setUserId(data["userid"]);
      await setAccountType(data["account_type"]);
      showSnackBar(_scaffoldKey, 'Successfully Login');
      Navigator.of(context).pushNamedAndRemoveUntil(
          Home.myRoute, (Route<dynamic> route) => false);
    }
  }
}
