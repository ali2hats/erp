import 'package:flutter/material.dart';
import 'package:erp_app/utility/ensure_visible.dart';

Widget buildTextField(
    {label,
    cObsecure,
    cFocusNode,
    cController,
    Function cOnSave,
    Function cValidator}) {
  return EnsureVisibleWhenFocused(
    focusNode: cFocusNode,
    child: TextFormField(
      obscureText: cObsecure != null ? true : false,
      focusNode: cFocusNode,
      decoration: InputDecoration(
          labelText: label,
          border: OutlineInputBorder(
            borderRadius: new BorderRadius.circular(25.0),
          )),
      controller: cController,
      onSaved: (String value) {
        if (cOnSave != null) {
          cOnSave(value);
        }
      },
      validator: (value) {
        if (cValidator != null) {
          return cValidator(value);
        } else {
          return null;
        }
      },
    ),
  );
}
