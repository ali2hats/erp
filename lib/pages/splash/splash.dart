import 'package:erp_app/pages/home/home.dart';
import 'package:erp_app/pages/login/login.dart';
import 'package:erp_app/utility/shared_preference.dart';
import 'package:flutter/material.dart';
import 'dart:async';

class Splash extends StatefulWidget {
  static const String myRoute = '/';
  @override
  State<StatefulWidget> createState() {
    return _SplashState();
  }
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () {
      checkTocken();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('2Hats Logic Solutions'),
      ),
    );
  }

  checkTocken() async {
    String token = await getToken();
    String route = Home.myRoute;
    if (token == '0') {
      route = LogIn.myRoute;
    }
    Navigator.of(context)
        .pushNamedAndRemoveUntil(route, (Route<dynamic> route) => false);
  }
}
