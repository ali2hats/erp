import 'dart:convert';
import 'dart:async';
import 'package:erp_app/main.dart';
import 'package:erp_app/pages/login/login.dart';
import 'package:erp_app/pages/task/task_home.dart';
import 'package:erp_app/pages/widget/elements.dart';
import 'package:erp_app/utility/shared_preference.dart';
import 'package:erp_app/utility/snak_bar.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:http/http.dart' as http;

class Home extends StatefulWidget {
  static const String myRoute = '/home';

  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    _fetchDataFromServer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Home'),
        ),
        drawer: _buildDrawer(),
        body: _isloading ? _loadingWidget : _getMainWidget());
  }

  _fetchDataFromServer() async {
    String myToken = await getToken();
    String myUrl = MyApp.baseUrl + "home.php";
    http.Response response = await http.get(myUrl, headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $myToken'
    });

    var errorStatus = response.body.contains('error');
    print(response.body);
    var data = json.decode(response.body);

    if (errorStatus) {
      print('data : ${data["error"]}');
      showSnackBar(_scaffoldKey, 'Please Login Again');
      setToken('0');
      Timer(Duration(seconds: 1), () {
        Navigator.of(context).pushNamedAndRemoveUntil(
            LogIn.myRoute, (Route<dynamic> route) => false);
      });
    } else {
      _messages.putIfAbsent('task_info', () => data["task_info"]);
      _messages.putIfAbsent('task_time', () => data["task_time"]);
      _messages.putIfAbsent('timesheet_info', () => data["timesheet_info"]);

      Map<String, dynamic> passedPieValues = data["pie_data"];

      passedPieValues.forEach((pieKey, pieVal) {
        pieData.putIfAbsent(pieKey, () => pieVal.toDouble());
      });

      setState(() {
        _isloading = false;
      });
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool _isloading = true;

  static Map<String, String> _messages = new Map();

  static Map<String, double> pieData = new Map();

  Widget _buildDrawer() {
    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Text(
              'Home',
              style: TextStyle(color: Colors.white, fontSize: 22),
            ),
            decoration: BoxDecoration(color: Colors.blue),
          ),
          ListTile(
            title: createNormalText('Task'),
            onTap: () {
              Navigator.pushNamed(context, TaskHome.myRoute);
            },
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            title: createNormalText('Project'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            title: createNormalText('Timesheet'),
            onTap: () {},
          ),
          Divider(
            color: Colors.grey,
          ),
          ListTile(
            title: createNormalText('Log Out'),
            onTap: () {
              setToken('0');
              Navigator.of(context).pushNamedAndRemoveUntil(
                  LogIn.myRoute, (Route<dynamic> route) => false);
            },
          ),
        ],
      ),
    );
  }

  Widget _loadingWidget = new Center(
    child: new CircularProgressIndicator(),
  );

  Widget _createPieChart() {
    return PieChart(
      dataMap: pieData,
      legendStyle:
          defaultChartValueStyle.copyWith(color: Colors.green, fontSize: 22),
      chartValueStyle:
          defaultChartValueStyle.copyWith(color: Colors.green, fontSize: 22),
      colorList: colorList,
      showChartValuesOutside: true,
      chartType: ChartType.ring,
      chartRadius: MediaQuery.of(context).size.width / 2,
      showChartValuesInPercentage: true,
    );
  }

  static List<Color> colorList = [
    Colors.red,
    Colors.blue,
    Colors.orange,
  ];

  Widget _getMainWidget() {
    return Padding(
      padding: EdgeInsets.all(15),
      child: ListView(
        children: <Widget>[
          createNormalText(_messages['task_info']),
          SizedBox(
            height: 10,
          ),
          _createPieChart(),
          SizedBox(
            height: 20,
          ),
          createNormalText(_messages['task_time']),
          RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, TaskHome.myRoute);
            },
            child: Text('View my Task'),
          ),
          SizedBox(
            height: 10,
          ),
          createNormalText(_messages['timesheet_info']),
          RaisedButton(
            onPressed: () {},
            child: Text('View my Timesheet'),
          ),
        ],
      ),
    );
  }
}
