import 'package:flutter/material.dart';

Widget createNormalText(String string) {
  return Text(
    string,
    style: TextStyle(color: Colors.black, fontSize: 18),
  );
}
