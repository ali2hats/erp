import 'package:erp_app/main.dart';
import 'package:erp_app/pages/widget/elements.dart';
import 'package:erp_app/utility/snak_bar.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
import 'package:erp_app/utility/shared_preference.dart';
import '../login/login.dart';

class DetailTab extends StatefulWidget {
  final String taskId;
  var _scaffoldKey;
  DetailTab(this.taskId, this._scaffoldKey);
  @override
  State<StatefulWidget> createState() {
    return _DetailTabState(taskId, _scaffoldKey);
  }
}

class _DetailTabState extends State<DetailTab> {
  bool _isloading = true;
  final String taskId;
  var _scaffoldKey;
  _DetailTabState(this.taskId, this._scaffoldKey);

  @override
  void initState() {
    super.initState();
    _fetchDataFromServer();
  }

  _fetchDataFromServer() async {
    String myToken = await getToken();
    String myUrl = MyApp.baseUrl + "task_details.php";

    final response = await http.post(myUrl, headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $myToken'
    }, body: {
      "task_id": taskId
    });

    var errorStatus = response.body.contains('error');
    print(response.body);

    taskDetail = json.decode(response.body);

    if (errorStatus) {
      print('data : ${taskDetail["error"]}');
      showSnackBar(_scaffoldKey, 'Please Login Again');
      setToken('0');
      Timer(Duration(seconds: 1), () {
        Navigator.of(context).pushNamedAndRemoveUntil(
            LogIn.myRoute, (Route<dynamic> route) => false);
      });
    } else {
      setState(() {
        _isloading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return _isloading ? _loadingWidget : _buildBody();
  }

  Widget _loadingWidget = new Center(
    child: new CircularProgressIndicator(),
  );

  Map<String, dynamic> taskDetail;
  
  Widget _buildBody() {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              createNormalText('Id :' + taskId),
              createNormalText(taskDetail['priority'])
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            children: <Widget>[
              createNormalText('Created Date'),
              createNormalText(taskDetail['created_date']),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
          SizedBox(
            height: 30,
          ),
          LinearPercentIndicator(
            animation: true,
            lineHeight: 20.0,
            animationDuration: 2500,
            percent: double.parse(taskDetail['percentage_completed']),
            center: Text((double.parse(taskDetail['percentage_completed']) * 100)
                    .toStringAsFixed(1) +
                "% Done"),
            linearStrokeCap: LinearStrokeCap.roundAll,
            progressColor: Colors.green,
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              createNormalText('Estimated Time'),
              createNormalText(taskDetail['estimated_time']),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              createNormalText('Time Spent'),
              createNormalText(taskDetail['time_spent']),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              createNormalText('Start Date'),
              createNormalText(taskDetail['start_date']),
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              createNormalText('Deadline'),
              createNormalText(taskDetail['deadline'])
            ],
          ),
          SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              createNormalText('Project Name'),
              createNormalText(taskDetail['project_name']),
              RaisedButton(
                child: Text('View Poject'),
                onPressed: () {},
              )
            ],
          ),
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
