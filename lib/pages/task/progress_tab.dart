import 'package:flutter/material.dart';

class ProgressTab extends StatefulWidget {
  final String taskId;
  ProgressTab(this.taskId);
  @override
  State<StatefulWidget> createState() {
    return _ProgressTabState(taskId);
  }
}

class _ProgressTabState extends State<ProgressTab> {
  final String taskId;
  _ProgressTabState(this.taskId);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text('Progress'),
    );
  }
}
