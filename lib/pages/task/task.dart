import 'package:erp_app/pages/task/hours_tab.dart';
import 'package:erp_app/pages/task/progress_tab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './detail_tab.dart';

class Task extends StatefulWidget {
  static const String myRoute = '/task';
  @override
  State<StatefulWidget> createState() {
    return _TaskState();
  }
}

final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

class _TaskState extends State<Task> {
  @override
  Widget build(BuildContext context) {
    final String passedTaskId = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Task'),
      ),
      body: _buildBody(passedTaskId),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.info_outline),
            title: Text('Details'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.timelapse),
            title: Text('Hours'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.timeline),
            title: Text('Add Progress'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }

  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget _buildBody(passedTaskId) {
    switch (_selectedIndex) {
      case 1:
        return HoursTab(passedTaskId);
        break;
      case 2:
        return ProgressTab(passedTaskId);
        break;
      default:
        return DetailTab(passedTaskId, _scaffoldKey);
        break;
    }
  }
}
