import 'package:erp_app/main.dart';
import 'package:erp_app/pages/login/login.dart';
import 'package:erp_app/pages/task/task.dart';
import 'package:erp_app/pages/widget/elements.dart';
import 'package:erp_app/utility/shared_preference.dart';
import 'package:erp_app/utility/snak_bar.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class TaskHome extends StatefulWidget {
  static const String myRoute = '/task_home';

  @override
  State<StatefulWidget> createState() {
    return _TaskHomeState();
  }
}

class _TaskHomeState extends State<TaskHome> {
  @override
  void initState() {
    super.initState();
    _fetchDataFromServer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text('Task'),
        ),
        body: _isloading ? _loadingWidget : _buildBody(context));
  }

  _fetchDataFromServer() async {
    String myToken = await getToken();
    String myUrl = MyApp.baseUrl + "task.php";
    http.Response response = await http.get(myUrl, headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $myToken'
    });

    var errorStatus = response.body.contains('error');
    print(response.body);

    var data = json.decode(response.body);

    if (errorStatus) {
      print('data : ${data["error"]}');
      showSnackBar(_scaffoldKey, 'Please Login Again');
      setToken('0');
      Timer(Duration(seconds: 1), () {
        Navigator.of(context).pushNamedAndRemoveUntil(
            LogIn.myRoute, (Route<dynamic> route) => false);
      });
    } else {
      tasks = data['tasks'];
      setState(() {
        _isloading = false;
      });
    }
  }

  bool _isloading = true;
  
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<dynamic> tasks = new List();

  Widget _loadingWidget = new Center(
    child: new CircularProgressIndicator(),
  );

  Widget _buildBody(context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Expanded(
                child: TextField(
                  onChanged: (text) {
                    print("ChangedText: $text");
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(25.0)),
                      hintText: 'Search'),
                ),
              ),
              IconButton(
                icon: Icon(Icons.filter_list),
                onPressed: () {
                  _showBottomSheetForFilter(context);
                },
              )
            ],
          ),
        ),
        SizedBox(
          height: 4,
        ),
        Divider(
          color: Colors.grey,
        ),
        Expanded(
          child: ListView.builder(
            itemCount: tasks.length,
            itemBuilder: (BuildContext context, int index) {
              return _buildTaskItem(index);
            },
          ),
        )
      ],
    );
  }

  Widget _buildTaskItem(int index) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          createNormalText(tasks[index]['date']),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  createNormalText(tasks[index]['task_title']),
                  createNormalText('Prority :' + tasks[index]['priority'])
                ],
              ),
              IconButton(
                icon: Icon(
                  Icons.last_page,
                  size: 31,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, Task.myRoute,
                      arguments: tasks[index]['task_id']);
                },
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          LinearPercentIndicator(
            animation: true,
            lineHeight: 20.0,
            animationDuration: 2500,
            percent: double.parse(tasks[index]['percentage_done']),
            center: Text((double.parse(tasks[index]['percentage_done']) * 100)
                    .toStringAsFixed(1) +
                "% Done"),
            linearStrokeCap: LinearStrokeCap.roundAll,
            progressColor: Colors.green,
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            color: Colors.grey,
          ),
        ],
      ),
    );
  }

  String _currentFilterValue = 'task';
  void _showBottomSheetForFilter(context) async {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return Container(
              child: new Wrap(
                children: <Widget>[
                  RadioListTile(
                    title: Text('Task'),
                    groupValue: _currentFilterValue,
                    value: 'task',
                    onChanged: (val) {
                      setState(() {
                        _currentFilterValue = val;
                      });
                    },
                  ),
                  RadioListTile(
                    title: Text('Project'),
                    groupValue: _currentFilterValue,
                    value: 'project',
                    onChanged: (val) {
                      setState(() {
                        _currentFilterValue = val;
                      });
                    },
                  ),
                  RadioListTile(
                    title: Text('Date'),
                    groupValue: _currentFilterValue,
                    value: 'date',
                    onChanged: (val) {
                      setState(() {
                        _currentFilterValue = val;
                      });
                    },
                  ),
                  RadioListTile(
                    title: Text('Priority'),
                    groupValue: _currentFilterValue,
                    value: 'priority',
                    onChanged: (val) {
                      setState(() {
                        _currentFilterValue = val;
                      });
                    },
                  ),
                ],
              ),
            );
          });
        });
  }
}
